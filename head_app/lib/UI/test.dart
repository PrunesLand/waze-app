import 'package:flutter/material.dart';

class TestingScreen extends StatelessWidget {
  TestingScreen({Key? key}) : super(key: key);

  final List<String> arry = ['item1', 'item2', 'item3'];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ...arry.map((item) {
            return Text(item);
          })
        ],
      ),
    );
  }
}
